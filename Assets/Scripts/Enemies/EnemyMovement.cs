﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;

public class EnemyMovement : MonoBehaviour
{
    Transform player;
    //PlayerHealth playerHealth;
    EnemyHealth enemyHealth;
    UnityEngine.AI.NavMeshAgent nav;

	public int compteur = 0;
	public string SceneName;


    void Awake ()
    {
        player = GameObject.FindGameObjectWithTag ("Player").transform;
       // playerHealth = player.GetComponent <PlayerHealth> ();
        enemyHealth = GetComponent <EnemyHealth> ();
        nav = GetComponent <UnityEngine.AI.NavMeshAgent> ();
    }


    void Update ()
    {
       /* if(enemyHealth.currentHealth > 0 && playerHealth.currentHealth > 0)
        {*/
             nav.SetDestination (player.position);
       /* }
        else
        {
           nav.enabled = false;
        }*/
    }

	void OnTriggerEnter(Collider other)
	{
		if (other.gameObject.CompareTag ("Player")) {
			compteur = compteur + 1;
		}

		if (compteur == 5) {
			SceneManager.LoadScene (SceneName);
		}
	}
}
