﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AudioController : MonoBehaviour {

	public AudioClip Jane;
	public AudioClip Statuette;
	public AudioClip Rouages;
	public AudioClip Fish;
	public AudioClip Bouteille;
	public AudioClip Squelette;


	private AudioSource source;

	bool sonStatuetteJoue = false;
	bool sonRouagesJoue = false;
	bool sonJaneJoue = false;
	bool sonFishJoue = false;
	bool sonBouteilleJoue = false;
	bool sonSqueletteJoue = false;



	void Awake () {
		source = GetComponent<AudioSource>();
	}

	void OnTriggerEnter(Collider other)
	{

		if (other.gameObject.CompareTag ("Player") && sonStatuetteJoue == false && this.CompareTag("Statuette")) {
			source.PlayOneShot (Statuette);
			sonStatuetteJoue = true;
		}

		if (other.gameObject.CompareTag ("Player") && sonRouagesJoue == false && this.CompareTag("Rouages")) {
			source.PlayOneShot (Rouages);
			sonRouagesJoue = true;
		}

		if (other.gameObject.CompareTag ("Player") && sonJaneJoue == false && this.CompareTag("Jane")) {
			source.PlayOneShot (Jane);
			sonJaneJoue = true;
		}

		if (other.gameObject.CompareTag ("Player") && sonFishJoue == false && this.CompareTag("Poisson")) {
			source.PlayOneShot (Fish);
			sonFishJoue = true;
		}

		if (other.gameObject.CompareTag ("Player") && sonBouteilleJoue == false && this.CompareTag("Bouteille")) {
			source.PlayOneShot (Bouteille);
			sonBouteilleJoue = true;
		}

		if (other.gameObject.CompareTag ("Player") && sonSqueletteJoue == false && this.CompareTag("EtapeFin")) {
			source.PlayOneShot (Squelette);
			sonSqueletteJoue = true;
		}
	}
}
