﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.SceneManagement;

public class PlayerController : MonoBehaviour {
	//Array ennemies;
	private GameObject FirstWall;
	private GameObject Poisson;
	private GameObject EtapeFin;
	private GameObject GrottePorte;
	private GameObject GrotteSortie;
	private GameObject Bouteille;
	private GameObject SecondWall;
	private GameObject ThirdWall;
	private GameObject EnnemyGo;
	private GameObject Ennemy;
	private GameObject EnnemyTwo;
	private GameObject EnnemyThree;
	private GameObject EnnemyFour;
	private GameObject WinEnd;

	public string SceneName;

	private bool PorteOuverte = false;


	// Use this for initialization
	void Start () {
		FirstWall = GameObject.FindWithTag ("FirstWall");
		Poisson = GameObject.FindWithTag ("Poisson");
		EtapeFin = GameObject.FindWithTag ("EtapeFin");
		GrottePorte = GameObject.FindWithTag ("GrottePorte");
		GrotteSortie = GameObject.FindWithTag ("GrotteSortie");
		Bouteille = GameObject.FindWithTag ("Bouteille");
		SecondWall = GameObject.FindWithTag ("SecondWall");
		ThirdWall = GameObject.FindWithTag ("ThirdWall");
		EnnemyGo = GameObject.FindWithTag ("EnnemyGo");
		Ennemy = GameObject.FindWithTag ("ennemy");
		EnnemyTwo = GameObject.FindWithTag ("ennemyTwo");
		EnnemyThree = GameObject.FindWithTag ("ennemyThree");
		EnnemyFour = GameObject.FindWithTag ("ennemyFour");
		WinEnd = GameObject.FindWithTag ("End");


		Poisson.SetActive (false);
		EtapeFin.SetActive (false);
		GrottePorte.SetActive (false);
		GrotteSortie.SetActive (false);
		Bouteille.SetActive (false);
		EnnemyGo.SetActive (false);
		Ennemy.SetActive (false);
		EnnemyTwo.SetActive (false);
		EnnemyThree.SetActive (false);
		EnnemyFour.SetActive (false);
		WinEnd.SetActive (false);

	}
	
	// Update is called once per frame
	void Update () {
		/*for(var go = GameObject in ennemies) {
		}
		*/
	}

	void OnTriggerEnter(Collider other)
	{
		if(other.gameObject.CompareTag("Statuette"))
		{
			//other.gameObject.SetActive (false);
			FirstWall.SetActive (false);
			GrottePorte.SetActive (false);
			//ici ecran noir avec la voix
		}

		if(other.gameObject.CompareTag("Rouages") && PorteOuverte == false)
		{
			//other.gameObject.SetActive (false);
			GrottePorte.SetActive (true);
			GrotteSortie.SetActive (false);
			Poisson.SetActive (true);
			//ici on entend la voix de Jane
			PorteOuverte = true;
		}

		if(other.gameObject.CompareTag("GrotteSortie"))
		{
			other.gameObject.SetActive (false);
			//ici on entend la voix de Jane
		}

		if(other.gameObject.CompareTag("Poisson"))
		{
			//other.gameObject.SetActive (false);
			Bouteille.SetActive (true);
			SecondWall.SetActive (false);
			//ici il faudra lancer les sons des monstres et je ne sais plus
		}

		if (other.gameObject.CompareTag ("Bouteille")) {
			//other.gameObject.SetActive (false);
			ThirdWall.SetActive (false);
			EtapeFin.SetActive (true);
		}

		if (other.gameObject.CompareTag ("EtapeFin")) {
			//other.gameObject.SetActive (false);
			EnnemyGo.SetActive (true);
		}

		if (other.gameObject.CompareTag ("EnnemyGo")) {
			other.gameObject.SetActive (false);
			Ennemy.SetActive (true);
			EnnemyTwo.SetActive (true);
			EnnemyThree.SetActive (true);
			EnnemyFour.SetActive (true);
			GrottePorte.SetActive (false);
			WinEnd.SetActive (true);
		}

		if(other.gameObject.CompareTag("End")) {
			SceneManager.LoadScene(SceneName);
		}

	}
}
