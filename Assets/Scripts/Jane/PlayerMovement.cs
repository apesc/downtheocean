﻿using UnityEngine;

public class PlayerMovement : MonoBehaviour
{

	private float speed_orientation = 12;
	bool Course =false;
    public float Speed = 8f;

	public AudioClip SoundSplash;
	public AudioClip SoundUnderwater;
	public AudioClip Statuette;

    Vector3 movement;
   // Animator anim;
    Rigidbody playerRigidbody;
    //int floorMask;
  //float camRayLength = 100f;

	void OnEnable()
	{

		if (GetComponent<AudioSource>().isPlaying == false) GetComponent<AudioSource>().PlayOneShot(SoundSplash);
	}

	void Update()
	{
		if (GetComponent<AudioSource> ().isPlaying == false)
			GetComponent<AudioSource> ().PlayOneShot (SoundUnderwater);

		if (Input.GetKeyDown (KeyCode.LeftShift) && Course == false) {
			Speed = 12;
		} else if (Input.GetKeyDown (KeyCode.LeftShift) && Course == true) {
			Speed = 8;
		}
	}

    void Awake()
    {
      //  floorMask = LayerMask.GetMask("Floor");
        //anim = GetComponent<Animator>();
        playerRigidbody = GetComponent<Rigidbody>();
    }

    void FixedUpdate()
    {
		float moveV = Input.GetAxisRaw("Vertical");
		float moveH = Input.GetAxisRaw ("Horizontal");


		if (Input.GetKey(KeyCode.D)) {
			 transform.Rotate(Vector3.up * speed_orientation * Time.deltaTime);
			}else if (Input.GetKey(KeyCode.Q)) {
			 transform.Rotate(-Vector3.up * speed_orientation * Time.deltaTime);
			}
				;

        Move(moveH, moveV);
     //  Turning();
       // Animating(moveH, moveV);
    }

    void Move(float moveH, float moveV)
    {
        movement.Set(moveH, 0f, moveV);

        movement = movement.normalized * Speed * Time.deltaTime;

        playerRigidbody.MovePosition(transform.position + movement);
    }
		

  /*  void Turning()
    {
       // Ray camRay = Camera.main.ScreenPointToRay(Input.mousePosition);

        //RaycastHit floorHit;

      //  if (Physics.Raycast(camRay, out floorHit, camRayLength, floorMask))
        //{
            Vector3 playerToMouse = transform.position;
            playerToMouse.y = 0f;

            Quaternion newRotation = Quaternion.LookRotation(playerToMouse);
            playerRigidbody.MoveRotation(newRotation);
        //}*/
    }

    /*void Animating(float moveH, float moveV)
    {
        bool walking = moveH != 0f || moveV != 0f;
        anim.SetBool("IsWalking", walking);
    }*/
//}
