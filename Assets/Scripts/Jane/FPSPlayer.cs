﻿using System;
using UnityEngine;

public class FPSPlayer : MonoBehaviour
{
	//Vector3 movement;
	//Rigidbody playerRigidbody;
	public bool Course = false;

    public float mouseSensitivity = 100.0f;
    public float clampAngle = 80.0f;

    private float rotY = 0.0f; 
    private float rotX = 0.0f;

    private CharacterController Cc;
    public float Gravity = 0.4f;
    public float Speed = 8;
    public AudioClip SoundSplash;
    public AudioClip SoundUnderwater;


	void Awake() {
		//playerRigidbody = GetComponent<Rigidbody> ();
	}

    void OnEnable()
    {
      
        if (GetComponent<AudioSource>().isPlaying == false) GetComponent<AudioSource>().PlayOneShot(SoundSplash);
    }

    void Start()
    {
        Cc = GetComponent<CharacterController>();
        Vector3 rot = transform.localRotation.eulerAngles;
        rotY = rot.y;
        rotX = rot.x;
    }

	void FixedUpdate() {
	
		//float moveH = Input.GetAxisRaw ("Horizontal");
		//float moveV = Input.GetAxisRaw ("Vertical");

		//Move(moveH, moveV);
	}

	/*void Move(float moveH, float moveV) {
		movement.Set (moveH, 0f, moveV);

		movement = movement.normalized * Speed * Time.deltaTime;

		playerRigidbody.MovePosition (transform.position + movement);
	}*/

    void Update()
    {
        if (GetComponent<AudioSource>().isPlaying == false) GetComponent<AudioSource>().PlayOneShot(SoundUnderwater);
        float mouseX = Input.GetAxis("Mouse X");
        float mouseY = -Input.GetAxis("Mouse Y");

        rotY += mouseX * mouseSensitivity * Time.deltaTime;
        rotX += mouseY * mouseSensitivity * Time.deltaTime;

        rotX = Mathf.Clamp(rotX, -clampAngle, clampAngle);

        Quaternion localRotation = Quaternion.Euler(rotX, rotY, 0.0f);
        transform.rotation = localRotation;

        Cc.Move(Vector3.up * -Gravity);

       

		if (Input.GetKey (KeyCode.Z)) {
			Cc.Move (transform.TransformDirection (Vector3.forward) * Speed * Time.deltaTime);        
		
		}

		if (Input.GetKey (KeyCode.S)) {
			Cc.Move (-transform.TransformDirection (Vector3.forward) * Speed * Time.deltaTime);        

		}

		if (Input.GetKey (KeyCode.D)) {
			Cc.Move (transform.TransformDirection (Vector3.right) * Speed * Time.deltaTime);        

		}

		if (Input.GetKey (KeyCode.Q)) {
			Cc.Move (-transform.TransformDirection (Vector3.right) * Speed * Time.deltaTime);        

		}

		if (Input.GetKey (KeyCode.LeftShift)) {
			Speed = 12;
		} else{
			Speed = 8;
		}



  
     }
}