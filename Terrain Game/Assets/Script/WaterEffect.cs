﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Characters.FirstPerson;

public class WaterEffect : MonoBehaviour {

    public GameObject Bubble;
    private FirstPersonController FPSScript;
    private FPSPlayer FPSPlayer; 



    // Use this for initialization
    void Start () {

        FPSScript = GetComponent<FirstPersonController>();
        FPSPlayer = GetComponent<FPSPlayer>();
        RenderSettings.fog = false;
        RenderSettings.fogColor = new Color32(44, 62, 80, 1);
        RenderSettings.fogDensity = 0.1f;
   
    }

   
    bool IsUnderWater()
    {
        return gameObject.transform.position.y < 230f;
   
    }
	
	// Update is called once per frame
	void Update () {
        RenderSettings.fog = IsUnderWater();

        if (IsUnderWater())
        {
            Bubble.SetActive(true);
            FPSScript.enabled = false;
            FPSPlayer.enabled = true; 
        }
        else // sur bateau 
        {
            Bubble.SetActive(false);
            FPSScript.enabled = true;
            FPSPlayer.enabled = false;
        }

	}
}
